jQuery('#email_widget_form').submit(function( event ) {
    payload['email'] = jQuery("input[name='email']").val();
    if (jQuery("input[name='secondary_optin']").prop("checked")){
        payload['secondary_optin'] = jQuery("input[name='secondary_optin']").val();
    }
    jQuery.ajax({
        url: ajaxurl,
        type: "POST",
        dataType: "json",
        data: {action:action,data:payload}

    }).done(function( data ) {
        if (data.success){
            jQuery('.email_form').fadeOut( "slow", function() {
                jQuery('.success').fadeIn();
            });
            jQuery('svg polygon').css("fill", "#8091DD")

        }else{
            showError()
        }
    }).fail(function() {
        showError()
    }) ;
    event.preventDefault();
});

function showError(){
    jQuery('.error').fadeIn('slow', function(){
        jQuery('.error').fadeOut('slow');
    });
}