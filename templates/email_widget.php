<!-- Feel free to change any of this -->
<script type="text/javascript">

  // The ajax endpoint information and payload schema
  var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
  var action = "generic_email_submit";
  var payload = {
     "email": "",
     "secondary_optin": 0,
  }
  
</script>

<div class="generic-email-widget">
    <div class="email_logo">
        <?php
       // echo '<img src="' . plugins_url( '../assets/svg/logo.svg', __FILE__ ) . '"  class="email_logo"> ';
        echo  file_get_contents(plugins_url( '../assets/svg/logo.svg', __FILE__ ));
        ?>
    </div>
    <div class="email_form">
        <form id="email_widget_form">
            Email <input type="email" name="email"><br>
            <input type="checkbox" name="secondary_optin" value="1" id="secondary_optin">
            <label for="secondary_optin">Sign up for a secondary newsletter</label><br/>
            <input type="submit" value="Submit">
        </form>
    </div>
    <div class="success" style="display:none;">
        Thank you for submitting your email, we'll be in touch!
    </div>
    <div class="error" style="display:none;">
        Sorry, there was an error while submitting your form, please try again.
    </div>
<div>


